module Meter (Beat, Meter, iamb, pentameter, checkLine, Fit(..)) where

import Data.List (maximumBy, groupBy)

import Vocab

data Fit = F Float [Float]
  deriving (Eq,Show)

instance Ord Fit where
  compare (F x xs) (F y ys) = compare x y

instance Monoid Fit where
  mempty = F 1 []
  mappend (F x xs) (F y ys) = F (x*y) (xs ++ ys)
  
zero k = F 0 (replicate k 0)

average :: [Float] -> Float
average xs = if length xs == 0 then 0 else sum xs / fromIntegral (length xs)

toFit xs = F (average xs) xs

data Meter = M {run :: [Beat] -> Fit, --given a sequence of syllable stresses, returns a score for how well it fits the meter
                maxlen :: Int}

instance Monoid Meter where
    f `mappend` g = M (\bs -> maximum [run f bs1 `mappend` run g bs2 | n <- [0..min (length bs) (maxlen f)], let (bs1,bs2) = splitAt n bs]) (maxlen f + maxlen g)
    mempty = M (\bs -> if null bs then mempty else zero (length bs)) 0
 
iamb' [Stressed] = toFit [0.6]
iamb' [Unstressed] = toFit [0.1]
iamb' [Unstressed,Stressed] = toFit [1,1]
iamb' [Stressed,Stressed] = toFit [0.9,1]
iamb' [Stressed,Unstressed] = toFit [0.7,0.7]
iamb' [Unstressed,Unstressed] = toFit [1,0.3]
iamb' [Unstressed,Unstressed,Stressed] = toFit [1,0.7,1]
iamb' [Unstressed,Stressed,Unstressed] = toFit [1,1,0.8]
iamb' bs = zero (length bs)

iamb = M iamb' 3

pentameter = mconcat (replicate 5 iamb)

checkLine :: Vocab -> Meter -> [String] -> Fit
checkLine v m ws = let prons = sequence (map v ws) in
                     maximum [ F x (map average (regroup p xs))| p <- prons, let F x xs = run m (concat p) ]
                   where regroup p xs = map (map fst) . collect [1..length ws] .
                                         zip xs . map fst .  concat . zipWith (map . (,)) [1..] $ p
                         collect [] xs = []
                         collect (n:ns) xs = let (xsn,xsrest) = span ((==n).snd) xs in xsn : collect ns xsrest