module Vocab (Vocab, Beat(..), parseVocab) where

import qualified Data.HashMap.Strict as M
import Data.Char (isAlpha, toUpper)

type Vocab = String -> [[Beat]]
data Beat = Unstressed | Stressed
    deriving (Eq, Ord,Show)

dummyVocab = const [[Stressed,Unstressed,Stressed],[Unstressed,Stressed]]

parseVocab :: [String] -> Vocab
parseVocab = parseLines M.empty
    where parseLines :: M.HashMap String [[Beat]] -> [String] -> Vocab
          parseLines m []     = m `seq` \w -> M.lookupDefault [[]] (map toUpper $ filter isAlpha w) m
          parseLines m (l:ls) = if head l /= ';' then parseLines (M.alter h word m) ls else parseLines m ls
            where (wordtext:pronels) = words l
                  word = filter isAlpha wordtext
                  pron = parsePron pronels
                  h Nothing   = Just [pron]
                  h (Just ps) = if pron `elem` ps then Just ps else Just (pron:ps)

parsePron :: [String] -> [Beat]
parsePron []       = []
parsePron (el:els)
    | '0' `elem` el  = Unstressed : parsePron els
    | '1' `elem` el  = Stressed : parsePron els
    | '2' `elem` el  = Stressed : parsePron els
    | otherwise      = parsePron els