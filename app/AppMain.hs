{-# LANGUAGE CPP #-}
#if defined(ghcjs_HOST_OS)
{-# LANGUAGE JavaScriptFFI #-}
#endif

module AppMain where

import Prelude hiding (read)

import GHCJS.DOM
import GHCJS.DOM.Types
import GHCJS.DOM.Document hiding (readyStateChange)
import GHCJS.DOM.Element
import GHCJS.DOM.NonElementParentNode
import GHCJS.DOM.Node
import GHCJS.DOM.EventM
import GHCJS.DOM.GlobalEventHandlers
import GHCJS.DOM.HTMLHyperlinkElementUtils
import GHCJS.DOM.HTMLTextAreaElement
import GHCJS.DOM.XMLHttpRequest

import Control.Concurrent
import Data.IVar.Simple
import Control.Exception (evaluate)

import Data.List (intercalate)
import Numeric (showHex)

import Meter
import Vocab

#if defined(ghcjs_HOST_OS)
loadVocab :: String -> JSM Vocab
loadVocab fn = do xhr <- newXMLHttpRequest
                  iv <- new
                  on xhr GHCJS.DOM.XMLHttpRequest.load $ do
                    txt <- getResponseTextUnchecked xhr
                    liftDOM $ write iv $ parseVocab (lines txt) 
                  openSimple xhr "GET" fn
                  send xhr
                  return (read iv)
                  
#else
loadVocab :: String -> JSM Vocab
loadVocab fn = liftIO $ readFile fn >>= return . parseVocab . lines
#endif

colour :: [String] -> Fit -> String
colour ws (F _ fs) = unwords $ zipWith colour' ws fs
    where colour' w f = "<span style=\"background-color:" ++ ftorgb f ++ "\">" ++ w ++ "</span>"
          ftorgb :: Float -> String
          ftorgb f = "#ff" ++ showHex' (floor (f * 256)) ++ showHex' (floor (f * 256))
          showHex' n = case showHex n "" of
                         [c] -> ['0',c]
                         [c1,c2] -> [c1,c2]
                         _ -> "ff"


appMain :: JSM ()
appMain = do
    v <- loadVocab "http://www.cs.nott.ac.uk/~pszjlh/cmudict-0.7b.txt"
    doc <- currentDocumentUnchecked
    body <- getElementByIdUnchecked doc "content"
    left <- createElement doc "div"
    setAttribute left "id" "input"
    appendChild body left
    loading <- createElement doc "p"
    createTextNode doc "Loading vocabulary..." >>= appendChild loading
    forkIO $ do v `seq` removeChild left loading
                return ()
    appendChild left loading
    textArea <- uncheckedCastTo HTMLTextAreaElement <$> createElement doc "textarea"
    appendChild left textArea
    right <- createElement doc "div"
    setAttribute right "id" "response"
    setInnerHTML right "Please enter a poem in box on the left."
    button <- uncheckedCastTo HTMLButtonElement <$> createElement doc "button"
    createTextNode doc "Click here!" >>= appendChild button
    on button click $ do
        text <- getValue textArea
        let ls = map words (lines text)
        let fs = map (checkLine v pentameter) ls
        let outs = zipWith colour ls fs
        let html = intercalate "<br/>" outs
        setInnerHTML right html
    appendChild left button
    appendChild body right
    return ()